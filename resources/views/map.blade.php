<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <title>Communify</title>
  <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <style>
    #map{
      -webkit-box-shadow: rgba(0,0,0,0.3) 0 1px 3px;
      -moz-box-shadow: rgba(0,0,0,0.3) 0 1px 3px;
      box-shadow: rgba(0,0,0,0.3) 0 1px 3px;
  	  border: 5px solid #f5f5f5;
  	  margin-top: 15px;
      margin-bottom: 20px;
    }
  </style>

</head>
<body>
  <center>
    <div id="map" style="width: 1200px; height: 500px;"></div>
    <button id="new_marker">New Spot</button>
  </center>

  <script type="text/javascript">

      var latlng = new google.maps.LatLng(60.169879, 24.938831);

      function addmarker(latilongi) {
        var marker = new google.maps.Marker({
            position: latilongi,
            title: 'new marker',
            draggable: true,
            map: map
        });
        map.setCenter(marker.getPosition());
        google.maps.event.addListener(marker, 'dragend', function(evt){
            $.post( "/location", { x: evt.latLng.lat().toFixed(3), y: evt.latLng.lng().toFixed(3) } );
            //alert('Lat: ' + evt.latLng.lat().toFixed(3) + ' - Lng: ' + evt.latLng.lng().toFixed(3));
            $.get( "/ranking", { x: marker.getPosition().lat(), y: marker.getPosition().lng() } ).done(function( data ) {
              //alert( "Data Loaded: " + data );
              var obj = jQuery.parseJSON(data);
              infowindow.setContent("<div style='margin: 18px'><div style='font-size: 20px; font-weight: bold; margin-bottom: 5px'>" + obj.message + "</div><div>" + obj.rankings + "</div></div>");
              infowindow.open(map, marker);
            });
            map.setZoom(16);
            map.setCenter(new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng()));
        });

        // google.maps.event.addListener(marker, 'click', (function(marker, i) {
        //   $.get( "/ranking", { x: marker.getPosition().lat(), y: marker.getPosition().lng() } ).done(function( data ) {
        //     var obj = jQuery.parseJSON(data);
        //     infowindow.setContent("<div style='margin: 18px'><div style='font-size: 20px; font-weight: bold; margin-bottom: 5px'>" + obj.message + "</div><div>" + obj.rankings + "</div></div>");
        //     infowindow.open(map, marker);
        //   });
        // })(marker, i));

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            $.get( "/ranking", { x: marker.getPosition().lat(), y: marker.getPosition().lng() } ).done(function( data ) {
              //alert( "Data Loaded: " + data );
              var obj = jQuery.parseJSON(data);
              infowindow.setContent("<div style='margin: 18px'><div style='font-size: 20px; font-weight: bold; margin-bottom: 5px'>" + obj.message + "</div><div>" + obj.rankings + "</div></div>");
              infowindow.open(map, marker);
            });
          }
        })(marker, i));


      }

      $(document).ready(function(){
        $("#new_marker").click(function() {
          addmarker(latlng);
        });
      });

      var locations = [
        ['', 60.172014, 24.936235, 1],
        ['', 60.170719, 24.955669, 2]
      ];

      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: new google.maps.LatLng(60.169879, 24.938831),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });

      var infowindow = new google.maps.InfoWindow();

      var marker, i;

      for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          draggable:true,
          map: map
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(locations[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));

        google.maps.event.addListener(marker, 'dragend', function(evt){
            //alert('Lat: ' + evt.latLng.lat().toFixed(3) + ' - Lng: ' + evt.latLng.lng().toFixed(3));
        });

      }

  </script>
</body>
