<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CriteriaCategory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'criteria_categories';
}
