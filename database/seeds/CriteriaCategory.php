<?php

use Illuminate\Database\Seeder;
use App\Criteria;
use App\CriteriaCategory as Category;

class CriteriaCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bisnodeCriteria = [
        	'bric_education_level' => [
        		'name' => 'Education',
        		'type' => 'category',
        		'options' => [
        			[
	        			'name' => 'Clever Clogs',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '3'
        			],
        			[
	        			'name' => 'Average Learners',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '1'
        			],
        			[
	        			'name' => 'Anti intellectuals',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '1'
        			]
        		]
        	],
        	'bric_life_stage' => [
        		'name' => 'Life Stage',
        		'type' => 'category',
        		'options' => [
        			[
	        			'name' => 'Elderly',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '4'
        			],
        			[
	        			'name' => 'Youthful',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '1'
        			],
        			[
	        			'name' => 'Settled',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '2'
        			]
        		]
        	],
        	'bric_purchasing_power' => [
        		'name' => 'Wealth',
        		'type' => 'category',
        		'options' => [
        			[
	        			'name' => 'Fairly Wealthy',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '2'
        			],
        			[
	        			'name' => 'Dirt Poor',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '1'
        			],
        			[
	        			'name' => 'Filthy Rich',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '4'
        			]
        		]
        	],
        	'language' => [
        		'name' => 'Language',
        		'type' => 'category',
        		'options' => [
        			[
	        			'name' => 'Finnish',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => 'fi'
        			],
        			[
	        			'name' => 'Swedish',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => 'sv'
        			]
        		]
        	],
        	/*'pred_family_with_children' => [
        		'name' => 'Families',
        		'type' => 'range',
        		'options' => [
        			[
	        			'name' => 'Families',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => ''
        			],
        			[
	        			'name' => 'No Families',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => ''
        			]
        		]
        	],
        	'pred_family_with_children_10_to_17' => [
        		'name' => 'Grown Up Families'
        		'type' => 'range',
        		'options' => [
        			[
	        			'name' => 'Grown Up Families',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => ''
        			],
        			[
	        			'name' => 'No Grown Up Families',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => ''
        			]
        		]
        	],
        	'pred_family_with_children_under_10' => [
        		'name' => 'Young Families'
        		'type' => 'range',
        		'options' => [
        			[
	        			'name' => 'Young Families',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => ''
        			],
        			[
	        			'name' => 'No Young Families',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => ''
        			]
        		]
        	],*/
        	'bric_home_ownership' => [
        		'name' => 'Home Ownership',
        		'type' => 'category',
        		'options' => [
        			[
	        			'name' => 'Home Owners',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '1'
        			],
        			[
	        			'name' => 'Renters',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '2'
        			]
        		]
        	],
        	'bric_housing_type' => [
        		'name' => 'Housing Type',
        		'type' => 'category',
        		'options' => [
        			[
	        			'name' => 'Homes',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '1'
        			],
        			[
	        			'name' => 'Flats',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '2'
        			]
        		]
        	]
/*        	'pred_car_ownership' => [
        		'name' => 'Cars',
        		'type' => 'range',
        		'options' => [
        			[
	        			'name' => 'Drivers',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => ''
        			],
        			[
	        			'name' => 'Pedestrians',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => ''
        			]
        		]
        	],*/
        	'valuegraphics_9_classes' => [
        		'name' => 'Personalities',
        		'type' => 'category',
        		'options' => [
        			[
	        			'name' => 'Homebodies',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '1'
        			],
        			[
	        			'name' => 'Traditionalists',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '2'
        			], 
        			[
	        			'name' => 'Role-model seeking nonconformist',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '3'
        			],
        			[
	        			'name' => 'Fact-centrics',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '4'
        			],  
        			[
	        			'name' => 'Open explorers',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '5'
        			],
        			[
	        			'name' => 'Influencers',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '6'
        			],
        			[
	        			'name' => 'Experience seekers',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '7'
        			], 
        			[
	        			'name' => 'Daredevils',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '8'
        			],
        			[
	        			'name' => 'People in the value center',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '9'
        			]
        		]         			       			
        	],
        	'suomi_360' => [
        		'name' => 'Personal Circumstance',
        		'type' => 'category',
        		'options' => [
        			[
	        			'name' => 'Old loners',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '1'
        			],
        			[
	        			'name' => 'Old lovers',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '2'
        			],
        			[
	        			'name' => 'Middle Class, Middle Age',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '3'
        			],
        			[
	        			'name' => 'Tired Parents',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '4'
        			],
        			[
	        			'name' => 'Student',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '5'
        			],
        			[
	        			'name' => 'Rebeling Teenagers, Tired Parents',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '6'
        			],
        			[
	        			'name' => 'Childless Old Fogies',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '7'
        			],
        			[
	        			'name' => 'Young Professional',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '8'
        			]
        		]
        	],
        	'bric_residential_area' => [
        		'name' => 'Neighbourhood',
        		'type' => 'category',
        		'options' => [
        			[
	        			'name' => 'Countryside',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '1'
        			],
        			[
	        			'name' => 'Suburbs',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '3'
        			],
        			[
	        			'name' => 'City Center',
	        			'percentage_min' => 0,
	        			'percentage_max' => 0,
	        			'bisnode_id' => '5'
        			]
        		]
        	]
        ];

        foreach ($bisnodeCriteria as $key => $value) {
        	$criteria = Criteria::create([
	            'name' => $value['name'],
	            'type' => $value['type'],
	            'bisnode_name' => $key
	        ]);

	        foreach ($value['options'] as $categoryKey => $categoryValue) {

	        	$categoryValue['criteria_id'] = $criteria->id;
	        	Category::create($categoryValue);
	        }
        }
    }
}
