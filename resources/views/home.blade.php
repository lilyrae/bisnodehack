<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Find your Community</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="/selectize.js-master/examples/css/normalize.css">
        <link rel="stylesheet" href="/selectize.js-master/examples/css/stylesheet.css">
        <link rel="stylesheet" href="/selectize.js-master/dist/css/selectize.bootstrap2.css">
        <link rel="stylesheet" href="/jquery.popSelect.css">
        <style>
            @import url('https://getbootstrap.com/dist/css/bootstrap.css');

            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway';
                font-weight: bold;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
                font-weight: normal;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .tag-line {
                font-size: 28px;
            }

            .search-box {
                display:inline-block;
            }

            .name {
                font-size: 18px;
            }
            .search-tags .btn{
              font-weight: bold;
            }
            .popover-select-wrapper{
              width: 100% !important;
            }
            .popover-select-list li, .popover-select-list li:hover {
              background-color: #5cb85c;
              border-color: #4cae4c;
              color:#fff;
              border-radius: 3px;
            }
            .popover-select.bottom {
              left: 0 !important;
            }
            .popover-select {
              max-width: 100%;
            }
            .popover-select-tags{
              height: auto !important;
              min-height: 37px;
              margin: 5px 0;
            }
            .popover-select-tags .tag{
              background-color: #5cb85c;
              border-color: #4cae4c;
              border-radius: 3px;
            }
        </style>
    </head>
    <body class="container">
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Find your Community
                </div>

                <div class="row tag-line">
                    <div class="col-md-12">
                        Where do you belong?
                    </div>
                </div>
                <br>
                <br>

                <div class="row ">
                    <div class="col-md-11 search-box">
                        <!-- <select id="select-interests" class="font" placeholder="Interests, Hobbies, Life Style .."></select> -->
                        <select id="select-interests" class="font" multiple>
                          <option value="Elderly">Elderly</option>
                          <option value="Wealthy">Wealthy</option>
                          <option value="Young">Young</option>
                          <option value="Kids">Kids</option>
                          <option value="Single">Single</option>
                        </select>
                    </div>
                    <div class="col-md-1">
                        <a href="/map" class="btn btn-primary" style="font-weight:bold;font-size:14px;color:#fff;">Search</a>
                    </div>
                </div>
                <!-- <div class="row pull-left search-tags">
                  <div class="col-md-12">
                    <button type="button" class="btn btn-success btn-sm">Elderly</button>
                    <button type="button" class="btn btn-success btn-sm">Wealthy</button>
                    <button type="button" class="btn btn-success btn-sm">Young</button>
                    <button type="button" class="btn btn-success btn-sm">Kids</button>
                    <button type="button" class="btn btn-success btn-sm">Single</button>
                  </div>
                </div> -->
                <br>
                <br>

            </div>
        </div>
        <script src="/jquery.min.js"></script>
        <script src="/jquery.popSelect.js"></script>
        <script src="/selectize.js-master/dist/js/standalone/selectize.min.js"></script>
        <script type="text/javascript">
            // $( ".search-tags .btn" ).click(function() {
            //     $('#select-interests').val($(this).text());
            //     $(this).remove();
            // });

            $('#select-interests').popSelect({
              showTitle: false,
              placeholderText: 'Interests, Hobbies, Life Style ..',
              position: 'bottom'
              //autofocus: true
            });

            // $('#select-interests').selectize({
            //     valueField: 'title',
            //     labelField: 'title',
            //     searchField: 'title',
            //     maxItems: null,
            //     options: [],
            //     create: false,
            //     render: {
            //         option: function(item, escape) {
            //             return '<div>' +
            //                 '<span class="title">' +
            //                     '<span class="name">' + escape(item.title) + '</span>' +
            //                 '</span>' +
            //             '</div>';
            //         }
            //     },
            //     load: function(query, callback) {
            //         if (!query.length) return callback();
            //         $.ajax({
            //             url: '/facebook',
            //             type: 'GET',
            //             dataType: 'json',
            //             data: {
            //                 q: query
            //             },
            //             error: function() {
            //                 callback();
            //             },
            //             success: function(res) {
            //                 callback(res);
            //             }
            //         });
            //     }
            // });
        </script>
    </body>
</html>
