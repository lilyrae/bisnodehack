<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as GuzzleRequest;

class Page extends Controller
{
    /**
     * @return view
     */
    public function home()
    {
        return view('home');
    }

    /**
     * @return view
     */
    public function map()
    {
        return view('map');
    }
}
