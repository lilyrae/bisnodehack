<?php
namespace App;

use App\ExternalAPIs\TargetingSearchInterface;
use App\ExternalAPIs\TargetingSearchItem;
use GuzzleHttp\Client;

/**
 * Facebook targeting search
 *
 * Targeting types:
 *
 *   adcountry, adregion, adcity, adeducationschool, adeducationmajor, adlocale, adworkemployer, adkeyword, adzipcode,
 *   adgeolocation, audienceinterest
 */
class FacebookSearch
{
    const URI = 'https://graph.facebook.com/v2.5/search';

    /**
     * @var string
     */
    protected $token;

    public function __construct()
    {
        $this->token = 'CAAIX5ZAYs2VoBANY5dbYos26xShYna9yxSSxkuiKvsCnsAMZAj9lScZC2712Sk7d8yArGIw5uP2S9I9ZBkcsJ8DGy'
            . 'ZAHnYhcZCzKfkX7eQv2LWfD7CVZCvcNLzFuY9cLZBs5nioDdbbSNqOC8zID26mmGzrrcozO8QS2D98yBhfxgdw1MQaunSss';
    }

    /**
     * Search for targeting objects from Facebook
     *
     * @param string $type
     * @param mixed  $value
     *
     * @return mixed
     */
    public function search($value, $type= 'adinterest')
    {
        $guzzle   = new Client();
        $response = $guzzle->get(self::URI . '?' . $this->getQueryString($type, $value));

        return $this->handleResponse($response->getBody());
    }

    /**
     * @param string $type
     * @param mixed  $value
     *
     * @return string
     */
    protected function getQueryString($type, $value)
    {
        $query = [
            'type'         => $type,//$this->getFacebookType($type),
            'q'            => $value,
            'access_token' => $this->token
        ];

        return \GuzzleHttp\Psr7\build_query($query);
    }

    /**
     * Translate internal type to a Facebook type
     *
     * @param string $type
     *
     * @return string
     */
    protected function getFacebookType($type)
    {
        return in_array($type, ['city', 'country']) ? 'adgeolocation' : $type;
    }

    /**
     * @param string $response
     *
     * @return mixed
     */
    protected function handleResponse($response)
    {
        $responses = json_decode($response, JSON_OBJECT_AS_ARRAY)['data'];
        $items     = [];
        foreach ($responses as $response) {
            $item = new TargetingSearchItem;
            $item->setFacebookId(isset($response['key']) ? $response['key'] : $response['id'])
                ->setName($response['name'])
                ->setCountryCode(isset($response['country_code']) ? $response['country_code'] : null);
            $items[] = $item;
        }

        return $items;
    }
}
