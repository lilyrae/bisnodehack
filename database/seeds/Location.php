<?php

use Illuminate\Database\Seeder;
use App\Location as LocationModel;

class Location extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locations = [
        	[60.158796, 24.875530, 'Lauttasaari'],
			[60.156149, 24.914630, 'Jatkasaari'],
			[60.164319, 24.909325, 'Lapinlahti'],
			[60.157980, 24.951763, 'Kaivopuisto'],
			[60.164220, 24.938116, 'Punavuori'],
			[60.166945, 24.969932, 'Katajonki'],
			[60.156463, 24.933320, 'Eira'],
			[60.170579, 24.934302, 'Kammpi'],
			[60.182662, 24.918933, 'Toolo'],
			[60.187249, 24.951638, 'Kallio'],
			[60.206491, 24.974549, 'Arabia'],
			[60.197920, 24.931371, 'Pasila'],
			[60.186250, 25.006967, 'Kulosaari']
        ];

        foreach ($locations as $key => $location) {
        	LocationModel::create([
        		'name' => $location[2],
        		'x' => $location[0],
        		'y' => $location[1],
        		'code' => $location[2]
        	]);
        }
    }
}
