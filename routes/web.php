<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'Page@home');

Route::get('/test', 'Page@test');
Route::get('/map', 'Page@map');
Route::get('/facebook', 'Api@facebook');
Route::get('/location', 'Api@location');
Route::post('/location', 'Api@updateLocation');
Route::get('/ranking', 'Api@ranking');