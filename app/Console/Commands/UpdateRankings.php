<?php

namespace App\Console\Commands;

use App\Criteria;
use App\Location;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as GuzzleRequest;

class UpdateRankings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bisnode:update-rankings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the rankings of the different locations based on latest bisnode data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $criterion = Criteria::where('bisnode_name', 'bric_education_level')->get();
        $locations = Location::where('name', 'Kallio')->get();

        $client = new Client([
            'base_uri' => 'https://api-test.bisnode.fi',
            'timeout'  => 20.0,
        ]);

        $headers = ['x-api-key' => '306480X2E9226B949FC5053A8EA623D7327B4D7F295AD24'];

        foreach (/*Criteria::all()*/$criterion as $key => $criteria) {

            foreach (/*Location::all()*/$locations as $key => $location) {

/*                $body['variables'] = [$criteria->bisnode_name];
                echo $criteria->bisnode_name;
                echo $location->x;
                echo $location->y;

                $body['filters'] = [[
                                    "variable"=> "location", 
                                    "x"=> $location->x, 
                                    "y"=> $location->y, 
                                    "radius"=> 500,
                                    "unionGroup" => "location"
                                ]];*/
                $body = '{"variables":["bric_housing_type"],"filters":[{"variable":"bric_housing_type","value":[1,2]}]}';

                $request = new GuzzleRequest('POST', '/people/analytics/v2/query', $headers, $body);
                $response = $client->send($request, ['timeout' => 10]);
                echo $response->getBody();
                echo "\n";
                echo "\n";

            }
        }
    }
}
